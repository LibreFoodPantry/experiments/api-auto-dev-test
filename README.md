# Guest Info System API

The GuestInfoSystemAPI provides an OpenAPI specification that implemented
by the GuestInfoBackend and called by the GuestInfoFrontend. The developers
of the Backend and the Frontend are the clients of GuestInfoSystemAPI.

## Client Guide

Download the GuestInfoSystemAPI specification

* [Latest](bundles/openapi.yaml)
* [Previous versions](bundles/)

Each specification file is written in [OpenAPI](https://www.openapis.org/),
a well-adopted, standardized language based on [YAML](https://yaml.org/).
There are many 3rd-party tools that work with OpenAPI specifications that
may be useful to frontend or backend development. Here are some references
to get you started.

* [OpenAPI Tools](https://openapi.tools/)
* [The Swagger Viewer extension for VS Code](
  https://marketplace.visualstudio.com/items?itemName=Arjun.swagger-viewer)
  renders the specification locally.
* GitLab provides a nice view of OpenAPI specifications so long as they are
  stored in a single file named openapi.yaml.

## Developer Guide

Getting Started

1. Read [LibreFoodPantry.org](https://librefoodpantry.org/)
    and join its Discord server.
2. [Install development environment](docs/developer/install-development-environment.md)
3. Clone this repository, open it in VS Code and reopen it in a devcontainer.
4. [src/](src/) contains the specification split over files. Its entry point is
  [src/index.yaml](src/index.yaml).
5. Familiarize yourself with the systems used by this project (see docs below).
6. [Learn to build, test, and release the spec](docs/developer/build-test-and-release.md)

Development Infrastructure

* [Automated Testing](docs/developer/automated-testing.md)
* [Build System](docs/developer/build-system.md)
* [Dependency Management](docs/developer/dependency-management.md)
* [Development Environment](docs/developer/development-environment.md)
* [Documentation System](docs/developer/documentation-system.md)
* [Version Control System](docs/developer/version-control-system.md)
