# Automated Testing

We use npm to run our tests.

```bash
npm test
```

This calls the "test" script defined in package.json.
`npm test` should run all the tests (unit tests, end-to-end tests, documentation
tests, etc.). Other scripts may be defined to run individual tests.

## Linters

These automated tools help to ensure our files have the correct syntax,
are optimized, and are written using recommended practices.

### package.json Linter

To lint our package.json, we use [npm-package-json-lint](https://www.npmjs.com/package/npm-package-json-lint).
For more detailed configuration and usage instructions, see [the documentation](https://npmpackagejsonlint.org/docs).

### json Linter

For linting json files, we use [eslint](https://www.npmjs.com/package/eslint)
with the [associated json plugin](https://www.npmjs.com/package/eslint-plugin-json).
