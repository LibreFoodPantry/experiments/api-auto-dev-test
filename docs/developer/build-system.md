# Build System

We use npm to run our build.

```bash
npm run build
```

This calls the "build" script defined in package.json.
`npm run build` should build all artifacts that clients use.

## Tools in the build system

* swagger-cli - The build commands use `swagger-cli`.
    For more information, see [swagger-cli's documentation](https://github.com/APIDevTools/swagger-cli/blob/master/README.md).
