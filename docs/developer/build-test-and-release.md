# Build, Test, and Release the Spec

## Test

Validate the specification.

```bash
npm test
```

## Build

Bundle the specification in src/ into a single file, bundles/openapi.yaml .

```bash
npm run build
```

## Release

Copy bundles/openapi.yaml to a versioned file.

```bash
cp bundles/openapi.yaml bundles/guest-info-system-api.0.1.0.yaml
```
