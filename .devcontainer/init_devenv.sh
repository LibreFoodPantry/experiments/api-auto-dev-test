#!/bin/sh

## Remove existing node_modules so that it is regenerated with only
## those modules listed in package.json.
rm -rf node_modules

## Pin npm to versions 8.x.x
npm install --global npm@^8.10.0

## Only new shells will have the new version of npm.
## So run npm in a subshell to use the new version now.
bash -c "npm install"

## NPM completions
bash -c "npm completion >> ~/.bashrc"

echo
echo '================================================================================'
echo 'Before tab completions will work properly, you need to start a new bash shell.'
echo 'Start a new bash shell by pressing the following key chord: CTRL+SHIFT+`'
echo '================================================================================'
